
def multiply(a,b):
    """ This function multiply ab
    >>> multiply(2,3)
    6
    >>> multiply('a',2)
    'aa'
   """
    return a*b


def add(a, b):
    """ This function add ab
    >>> add(2,3)
    5
    >>> add('a','b')
    'ab'
   """
    return a + b





